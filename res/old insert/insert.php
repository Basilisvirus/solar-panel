<?php

$json = array();
$json["success"] = false;

// MySQL database credentials
DEFINE ('DBUSER', 'esp'); //Database User
DEFINE ('DBPW', 'esp123'); // Database Password
DEFINE ('DBHOST', 'localhost'); // Database Host
DEFINE ('DBNAME', 'esp2'); // Database Name

$dbc = mysqli_connect(DBHOST,DBUSER,DBPW);
if (!$dbc) { die("Database connection failed: " . mysqli_error($dbc)); }
$dbs = mysqli_select_db($dbc, DBNAME);
if (!$dbs) { die("Database selection failed: " . mysqli_error($dbc)); }

$hour=$_GET['hour'];
$minute=$_GET['minute'];
$s_t_c_Current=$_GET['s_t_c_Current'];
$c_t_b_Current=$_GET['c_t_b_Current'];
$pv_Voltage=$_GET['pv_Voltage'];

if( !empty($hour) && !empty($minute) & !empty($s_t_c_Current) & !empty($c_t_b_Current) & !empty($pv_Voltage) ){    
    $query  = sprintf("INSERT INTO measurements (hour, minute, s_t_c_Current, c_t_b_Current, pv_Voltage) VALUES ($hour, $minute, $s_t_c_Current, $c_t_b_Current, $pv_Voltage)");
	$result = mysqli_query($dbc, $query) or trigger_error("Query MySQL Error: " . mysqli_error($dbc));
    if($result){ $json["success"] = true; }
}    

$variable1 = mysqli_real_escape_string($dbc, isset($_POST['variable1']) && !empty($_POST['variable1']) ? $_POST['variable1'] : 0);

//LOG every HTTP POST call in file
$logtext = date("Y-m-d H:i:s")." Was query success? $ $result & variable1 value receive: $variable1;";

$myfile = fopen("logs.log", "a") or die("Unable to open file!");
          fwrite($myfile, $logtext."\n");
          fclose($myfile); exit();

mysqli_close($dbc); 

?>


<!DOCTYPE html>
<html>

<head>
    <title>
        Solar panel
    </title>
    <style>
        body {
            background-color: #00281f;
            color: white;
        }
    </style>
</head>

<body>
echo "Current from solar panel to charger is: "."$s_t_c_Current";
echo "Current from charger to battery is: "."$c_t_b_Current";
echo "solar panel voltage is: "."$pv_Voltage";
</body>
