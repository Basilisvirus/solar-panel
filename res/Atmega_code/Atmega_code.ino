
//calibration start button
int volatile button_cal = 5;
int volatile cal_done =0; //calibration is not done
int volatile cal_led = 6;

int volatile tick_counter =0; //used for interrupt counter
int volatile sec_counter =0; //used for seconds of interrupt
int volatile min_counter =0; //used for minutes counter
int volatile hour_counter = 0; //used for hour interrupt

//for itoa();
char s_to_c[30] ; //solar to charger current
char c_to_b[30] ;//charger to battery current
char v_solar[30];
char b_to_ic[30];

//temp variable save location
int volatile temp =0;

//compare var
int volatile comp =0;

void setup()
{
  //Interrupt mode
  TCCR0A = (1 << WGM01); //CTC mode (Clear Timer on Compare)
    
  //8-bit value that is continuously compared with the counter value (TCNT0)
  OCR0A = 156; //Total Timer Ticks, https://eleccelerator.com/avr-timer-calculator/ by setting Real Time in 0.01

  //Enable specific interrupt (the interrupt that we want to use)
  TIMSK0 = (1 << OCIE0A); //enable OCIE0A in TIMSK0 interrupt

 
  //Set prescaler 
  TCCR0B =  (1 << CS02) | (1<< CS00); //1024 prescaler set
  
 //Activate All interrupts
 SREG |= 0B10000000;

 
  Serial.begin(9600);

  pinMode(button_cal, INPUT_PULLUP);


   // Serial.println("Solar panel monitor");

//===========================Calibration


    //Wait for the button to start cal.
    temp = digitalRead(button_cal);
    Serial.println("Press button for calibration");
    
    while(temp == 1){
    temp = digitalRead(button_cal);  
    }

    Serial.println("cal buton pressed");
    //take one value for each sensor
    temp = analogRead(A0);
    itoa(temp, s_to_c,10);

    temp = analogRead(A1);
    itoa(temp, c_to_b,10);

    temp = analogRead(A2);
    itoa(temp, v_solar, 10);

    temp = analogRead(A3);
    itoa(temp, b_to_ic,10);
    
    //first save calibration values
	//Serial.println("#CALIBRATION ");
	Serial.print(min_counter);
	Serial.print(",");
	Serial.print(hour_counter);
	Serial.print(",");    
	Serial.print(s_to_c);
	Serial.print(",");
	Serial.print(c_to_b);
  Serial.print(",");
  Serial.print(v_solar);
  //Serial.print(" b_to_ic: ");
  //Serial.println(b_to_ic);
//Serial.println("Connect solar panel now, you have 1 minute");

//pointer to sec_counter
int *p = NULL;
p = &sec_counter;

//take sec_counter value
comp = sec_counter;

//make sure temp has no weird values
if(comp <=3){
  comp =58;
  if(*p <=3){
    *p = 1;
  }
}


//Serial.print("comp is ");
//Serial.println(comp);

/*
Serial.print("sec_counter is ");
Serial.println(sec_counter);



Serial.print("sec_counter is ");
Serial.println(*p);
*/

//Wait until temp (e.x. number 3) is equal to sec_counter (number 3), next minute.
while(comp != *p){
delay(400);
}

//reset timer
min_counter =0;
sec_counter =0;

//Serial.println("System start, counter set to zero");

  //#START tag
   // Serial.println("#START"); //START tag

cal_done =1;

}


void loop()
{


}


//Interrupt routine
ISR(TIMER0_COMPA_vect){


  tick_counter ++; // 1 interrupt activation

  //every 100 interrupts, 1 sec passes.
  if(tick_counter >=100){
    tick_counter =0;
    
    sec_counter++;
    
    //Serial.print(sec_counter);
    //Serial.println(" sec passed");
  }

  if(sec_counter >=60){
    sec_counter =0;
    
    min_counter ++;

    //Serial.print(min_counter);
    //Serial.println(" min passed");

//if cal is done
if(cal_done ==1){
//take values

    temp = analogRead(A0);
    itoa(temp, s_to_c,10);

    temp = analogRead(A1);
    itoa(temp, c_to_b,10);

    temp = analogRead(A2);
    itoa(temp, v_solar, 10);
    
    temp = analogRead(A3);
    itoa(temp, b_to_ic, 10);
          
      //Serial.print(",");
      Serial.print(min_counter);
      Serial.print(",");
      Serial.print(hour_counter);
      Serial.print(",");    
      Serial.print(s_to_c);
      Serial.print(",");
      Serial.print(c_to_b);
      Serial.print(",");
      Serial.print(v_solar);
      //Serial.print(" b_to_ic: ");
      //Serial.println(b_to_ic); 
     }
    
  }

  if(min_counter>=60){
    min_counter =0;
    hour_counter++;
    
    //Serial.print(hour_counter);
    //Serial.println(" min passed");
  }

}
