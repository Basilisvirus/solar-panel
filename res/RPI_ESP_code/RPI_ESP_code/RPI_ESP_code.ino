/*Cyber-Physical Systems, Team Energy
 
 Below are the two libraries needed for the wifi functionality.
 The WiFi.h one is included with the ESP32 board manager url which is https://dl.espressif.com/dl/package_esp32_index.json
 The ESP8266WiFi.h one is included with the ESP8266 board manager url which is https://arduino.esp8266.com/stable/package_esp8266com_index.json
 Make sure you add the corresponding url to you Arduino IDE according to the ESP board you plan to use
 In this case the WiFi.h library is uncommented, as we compile this script to work with the ESP32 Dev Board
*/

//#include <WiFi.h>
//#include<ESP8266WiFi.h>
#include "ESP8266WiFi.h"

char* ssid     = "LoRa-Guests";// Your WiFi SSID
char* password = "sensotube"; //Your WiFi password
char* host = "192.168.1.7"; // The host's IP address 

/*
const char* ssid = "";// Your WiFi SSID
const char* password = ""; //Your WiFi password
const char* host = ""; // The host's IP address
*/

//=======================================================================
//                    Power on setup
//=======================================================================

void setup() {
  Serial.begin(9600);
  delay(100);
Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Netmask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway: ");
  Serial.println(WiFi.gatewayIP());
  
}

//=======================================================================
//                    Main Program Loop
//=======================================================================
void loop() {
  String IncomingString = "";
  boolean StringReady="false";
  String variable1_string = "";
  String variable2_string = "";
  String variable3_string = "";
  String variable4_string = "";
  String variable5_string = "";
  
  while (IncomingString == ""){
    IncomingString = Serial.readString();
    if (IncomingString != ""){
      StringReady="true";
    }
  }
  if (StringReady){
	// Indexing the positions of the commas, the commas are used as delimiters
    int commaIndex = IncomingString.indexOf(',');
    //  Search for the next comma just after the first
    int secondCommaIndex = IncomingString.indexOf(',', commaIndex + 1);
    int thirdCommaIndex = IncomingString.indexOf(',', secondCommaIndex + 1);
    int fourthCommaIndex = IncomingString.indexOf(',',thirdCommaIndex + 1);
    //Splitting the String into multiple values, and assigning them to variables
    variable1_string = IncomingString.substring(0, commaIndex);
    variable2_string = IncomingString.substring(commaIndex + 1, secondCommaIndex);
    variable3_string = IncomingString.substring(secondCommaIndex + 1, thirdCommaIndex); 
    variable4_string = IncomingString.substring(thirdCommaIndex + 1, fourthCommaIndex);
    variable5_string = IncomingString.substring(fourthCommaIndex + 1);// To the end of the string
    
    float hour = variable1_string.toFloat();
    float minute = variable2_string.toFloat();
    float s_t_c_Current = variable3_string.toFloat();
    float c_t_b_Current = variable4_string.toFloat();
    float pv_Voltage = variable5_string.toFloat();

    Serial.println(hour);
    Serial.println(minute);
    Serial.println(s_t_c_Current);
    Serial.println(c_t_b_Current);
    Serial.println(pv_Voltage);
    
                
  
    Serial.print("connecting to ");
    Serial.println(host);

    WiFiClient client;
    const int httpPort = 80;
    if (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
    return;
    }
    
    String url1 = "/insert.php?";
    String url2 = "hour=";
    url2 += hour;
    url2 += "&minute=";
    url2 += minute;
    url2 += "&s_t_c_Current=";
    url2 += s_t_c_Current;
    String url3 = "&c_t_b_Current=";
    url3 += c_t_b_Current;
    url3 += "&pv_Voltage=";
    url3 += pv_Voltage;

    Serial.print("Requesting URL: ");
    Serial.println(url1+url2);

//Printing the entire URL
    client.print(String("GET ") + url1 + url2 + url3 + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
    delay(500);

    while (client.available()) {
      String line = client.readStringUntil('\r');
      Serial.print(line);
    }

    Serial.println();
    Serial.println("closing connection");
  }
}
//=======================================================================
