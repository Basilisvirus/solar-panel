EESchema Schematic File Version 4
LIBS:solar panel-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Environmental_devices:Solar_panel S1
U 1 1 5CF67B07
P 1900 3100
F 0 "S1" H 2000 4550 50  0000 C CNN
F 1 "Solar_panel" H 2000 4450 50  0000 C CNN
F 2 "Environmental_devices:Solar_panel" H 1800 4100 50  0001 C CNN
F 3 "" H 2150 3500 50  0001 C CNN
	1    1900 3100
	1    0    0    -1  
$EndComp
$Comp
L Resistors_through_hole:Res_th_hole U2
U 1 1 5CFB0D2F
P 2750 5750
F 0 "U2" V 2704 5838 50  0000 L CNN
F 1 "2.3Mohm" V 2795 5838 50  0000 L CNN
F 2 "Resistors_through_hole:Resistors_th_hole" H 2750 6095 50  0001 C CNN
F 3 "" H 2705 5750 50  0001 C CNN
	1    2750 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 4200 1850 5050
Wire Wire Line
	2550 4200 2550 4450
Wire Wire Line
	2550 4450 1250 4450
Wire Wire Line
	1250 4450 1250 4200
$Comp
L power:GND #PWR0101
U 1 1 5CFB710F
P 1250 4450
F 0 "#PWR0101" H 1250 4200 50  0001 C CNN
F 1 "GND" H 1255 4277 50  0000 C CNN
F 2 "" H 1250 4450 50  0001 C CNN
F 3 "" H 1250 4450 50  0001 C CNN
	1    1250 4450
	1    0    0    -1  
$EndComp
Connection ~ 1250 4450
Wire Wire Line
	2750 5050 1850 5050
Wire Wire Line
	2750 5450 2750 5500
Wire Wire Line
	2750 5950 2750 6100
$Comp
L power:GND #PWR0102
U 1 1 5CFB951C
P 2750 6100
F 0 "#PWR0102" H 2750 5850 50  0001 C CNN
F 1 "GND" H 2755 5927 50  0000 C CNN
F 2 "" H 2750 6100 50  0001 C CNN
F 3 "" H 2750 6100 50  0001 C CNN
	1    2750 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 5500 3350 5500
Connection ~ 2750 5500
Wire Wire Line
	2750 5500 2750 5550
$Comp
L power:GND #PWR0103
U 1 1 5CFC2F40
P 7300 6100
F 0 "#PWR0103" H 7300 5850 50  0001 C CNN
F 1 "GND" H 7305 5927 50  0000 C CNN
F 2 "" H 7300 6100 50  0001 C CNN
F 3 "" H 7300 6100 50  0001 C CNN
	1    7300 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5CFC328F
P 10000 6100
F 0 "#PWR0104" H 10000 5850 50  0001 C CNN
F 1 "GND" H 10005 5927 50  0000 C CNN
F 2 "" H 10000 6100 50  0001 C CNN
F 3 "" H 10000 6100 50  0001 C CNN
	1    10000 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5CFC3809
P 10050 4050
F 0 "#PWR0105" H 10050 3800 50  0001 C CNN
F 1 "GND" H 10055 3877 50  0000 C CNN
F 2 "" H 10050 4050 50  0001 C CNN
F 3 "" H 10050 4050 50  0001 C CNN
	1    10050 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5CFC3B3C
P 7450 4900
F 0 "#PWR0106" H 7450 4650 50  0001 C CNN
F 1 "GND" H 7455 4727 50  0000 C CNN
F 2 "" H 7450 4900 50  0001 C CNN
F 3 "" H 7450 4900 50  0001 C CNN
	1    7450 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 4050 10050 4000
Text Notes 1050 6550 0    50   ~ 0
Σύστημα ελέγχου τάσης και ρεύματος\n για την φόρτωση συσσωρευτών
Wire Notes Line
	950  6300 2650 6300
Wire Notes Line
	2650 6300 2650 6700
Wire Notes Line
	2650 6700 950  6700
Wire Notes Line
	950  6700 950  6300
Wire Wire Line
	10050 4000 9800 4000
Wire Wire Line
	7600 6100 7300 6100
Wire Wire Line
	7600 4900 7450 4900
Wire Wire Line
	10000 6100 9800 6100
$Comp
L Atmega328p_pu:ESP8266_AMICA_NodeMCU_DevBoard ESP1
U 1 1 5CFA42CA
P 8700 4300
F 0 "ESP1" H 8700 6665 50  0000 C CNN
F 1 "ESP8266_AMICA_NodeMCU_DevBoard" H 8700 6574 50  0000 C CNN
F 2 "Atmega328p_pu:ESP8266_AMICA_NodeMCU_DevBoard" H 8700 6100 50  0001 C CNN
F 3 "" H 8700 6100 50  0001 C CNN
	1    8700 4300
	1    0    0    -1  
$EndComp
Text Label 10000 5500 0    50   ~ 0
Atm_Tx
Wire Wire Line
	9800 5500 10000 5500
Text Label 10000 5800 0    50   ~ 0
Atm_Rx
Wire Wire Line
	9800 5800 10000 5800
Text Label 4150 3000 2    50   ~ 0
Atm_Tx
Text Label 4150 3300 2    50   ~ 0
Atm_Rx
Wire Wire Line
	4550 3300 4150 3300
$Comp
L Atmega328p_pu:Atmega328p_pu U3
U 1 1 5CFF93D0
P 5350 6200
F 0 "U3" H 5350 9967 50  0000 C CNN
F 1 "Atmega328p_pu" H 5350 9876 50  0000 C CNN
F 2 "Atmega328p_pu:Atmega328p_pu" H 5350 9950 50  0001 C CNN
F 3 "" H 5350 9950 50  0001 C CNN
	1    5350 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3000 4150 3000
Wire Notes Line
	2500 6950 2500 7300
Wire Wire Line
	1800 6700 1800 6950
Wire Notes Line
	1200 7300 1200 6950
Wire Notes Line
	2500 7300 1200 7300
Wire Notes Line
	1200 6950 2500 6950
Text Notes 1550 7150 0    50   ~ 0
Lead Acid Battery
Wire Notes Line
	6500 4100 6500 4300
Wire Notes Line
	6150 4200 6500 4200
Text Notes 6500 4200 0    50   ~ 0
solar to charger
Wire Notes Line
	7150 4100 7150 4300
Wire Notes Line
	6500 4300 7150 4300
Wire Notes Line
	6500 4100 7150 4100
Text Notes 6500 4250 0    50   ~ 0
current
Wire Notes Line
	6500 3800 6500 4000
Wire Notes Line
	6150 3900 6500 3900
Text Notes 6500 3900 0    50   ~ 0
charger to battery
Text Notes 6500 3950 0    50   ~ 0
current
Wire Notes Line
	7250 3800 7250 4000
Wire Notes Line
	6500 4000 7250 4000
Wire Notes Line
	6500 3800 7250 3800
Text Notes 3550 5700 0    50   ~ 0
calibration Button
Wire Notes Line
	4550 5700 4250 5700
Wire Notes Line
	4250 5600 3500 5600
Wire Notes Line
	3500 5600 3500 5800
Wire Notes Line
	3500 5800 4250 5800
Wire Notes Line
	4250 5600 4250 5800
$Comp
L Resistors_through_hole:Res_th_hole U1
U 1 1 5CFB06CC
P 2750 5250
F 0 "U1" V 2704 5338 50  0000 L CNN
F 1 "1MOhm" V 2795 5338 50  0000 L CNN
F 2 "Resistors_through_hole:Resistors_th_hole" H 2750 5595 50  0001 C CNN
F 3 "" H 2705 5250 50  0001 C CNN
	1    2750 5250
	0    1    1    0   
$EndComp
$Comp
L Resistors_through_hole:Res_th_hole U?
U 1 1 5D7CC97A
P 4100 5100
F 0 "U?" V 4054 5188 50  0000 L CNN
F 1 "1MOhm" V 4145 5188 50  0000 L CNN
F 2 "Resistors_through_hole:Resistors_th_hole" H 4100 5445 50  0001 C CNN
F 3 "" H 4055 5100 50  0001 C CNN
	1    4100 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 5300 4100 5550
Wire Wire Line
	4100 5550 4550 5550
Wire Wire Line
	4550 5550 4550 5700
$Comp
L power:+5V #PWR?
U 1 1 5D7DAC5E
P 4100 4300
F 0 "#PWR?" H 4100 4150 50  0001 C CNN
F 1 "+5V" H 4115 4473 50  0000 C CNN
F 2 "" H 4100 4300 50  0001 C CNN
F 3 "" H 4100 4300 50  0001 C CNN
	1    4100 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D7DD8D6
P 4450 4850
F 0 "#PWR?" H 4450 4600 50  0001 C CNN
F 1 "GND" H 4455 4677 50  0000 C CNN
F 2 "" H 4450 4850 50  0001 C CNN
F 3 "" H 4450 4850 50  0001 C CNN
	1    4450 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4800 4450 4800
Wire Wire Line
	4450 4800 4450 4850
Wire Wire Line
	4100 4300 4100 4500
Wire Wire Line
	4100 4500 4550 4500
Connection ~ 4100 4500
Wire Wire Line
	4100 4500 4100 4900
Wire Wire Line
	3350 5500 3350 3600
Wire Wire Line
	3350 3600 4550 3600
Wire Notes Line
	6500 3500 6500 3700
Wire Notes Line
	6150 3600 6500 3600
Text Notes 6500 3600 0    50   ~ 0
solar&battery to ICs
Text Notes 6500 3650 0    50   ~ 0
current
Wire Notes Line
	7300 3500 7300 3700
Wire Notes Line
	6500 3700 7300 3700
Wire Notes Line
	6500 3500 7300 3500
Connection ~ 1850 5050
Wire Wire Line
	1850 5050 1850 6300
$EndSCHEMATC
